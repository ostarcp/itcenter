<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ClassRoom;
use App\Teacher;
use App\ClassModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class XepPhong extends Model
{
    //
    protected $table = 'xep_phong';
    protected $fillable = ['tb1.id', 'tb1.class_id', 'tb1.room_id', 'tb1.teacher_id', 'tb1.day', 'tb1.start_time', 'tb1.end_time', 'tb1.mo_ta', 'tb1.created_at', 'tb1.updated_at'];

    public function room()
    {
        return $this->belongsTo(ClassRoom::class, 'room_id');
    }

    public function class()
    {
        return $this->belongsTo(ClassModel::class, 'class_id');
    }
    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function createStdClass()
    {
        $objItem = new \stdClass();
        foreach ($this->fillable as $field) {
            $field = substr($field, 4);
            $objItem->$field = null;
        }
        return $objItem;
    }


    public function loadOneTeacher($id_teacher)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.teacher_id', '=', $id_teacher)
            ->orderBy('tb1.day', 'asc')
            ->orderBy('tb1.start_time', 'asc');
        $list = $query->paginate(10, ['tb1.id']);
        return $list;
    }

    public function loadListWithPager($idClass)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.class_id', '=', $idClass)
            ->orderBy('tb1.day', 'asc')
            ->orderBy('tb1.start_time', 'asc');
        $list = $query->paginate(50, ['tb1.id']);
        return $list;
    }

    public function loadOne($id, $params = null)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.id', '=', $id);

        $obj = $query->first();
        return $obj;
    }

    public function findRecordByDay($day, $start_time, $end_time)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.day', '=', $day)
            ->where('tb1.start_time', '=', $start_time)
            ->where('tb1.end_time', '=', $end_time);

        $obj = $query->paginate(20);
        return $obj;
    }
    public function checkCa($day, $room_id)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.day', '=', $day)
            ->where('tb1.room_id', '=', $room_id);

        $obj = $query->paginate(20);
        return $obj;
    }

    public function checkExistRoom($room_id, $day, $start_time, $end_time)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.room_id', '=', $room_id)
            ->where('tb1.day', '=', $day)
            ->where('tb1.end_time', '>', $start_time)
            ->where('tb1.start_time', '<=', $start_time);
        // ->orWhere('tb1.start_time', '>', $end_time);

        $obj = $query->first();

        return $obj;
    }
    public function checkExistTeacher($id_teacher, $day, $start_time, $end_time)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.teacher_id', '=', $id_teacher)
            ->where('tb1.day', '=', $day)
            ->where('tb1.end_time', '>', $start_time)
            ->where('tb1.start_time', '<=', $start_time);

        $obj = $query->first();

        return $obj;
    }

    public function checkExistUpdate($room_id, $day, $start_time, $end_time)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.room_id', '=', $room_id)
            ->where('tb1.day', '=', $day)
            ->where('tb1.start_time', '=', $start_time);

        $obj = $query->first();

        return $obj;
    }

    public function checkUpdateExistRoom($id, $room_id, $day, $start_time, $end_time)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.room_id', '=', $room_id)
            ->where('tb1.day', '=', $day)
            ->where('tb1.end_time', '>', $start_time)
            ->where('tb1.start_time', '<=', $start_time)
            ->where('tb1.id', '!=', $id);

        $obj = $query->first();

        return $obj;
    }

    public function checkUpdateExistTeacher($id, $teacher_id, $day, $start_time, $end_time)
    {
        $query = DB::table($this->table . ' as tb1')
            ->select($this->fillable)
            ->where('tb1.teacher_id', '=', $teacher_id)
            ->where('tb1.day', '=', $day)
            ->where('tb1.end_time', '>', $start_time)
            ->where('tb1.start_time', '<=', $start_time)
            ->where('tb1.id', '!=', $id);

        $obj = $query->first();

        return $obj;
    }



    public function saveNew($params)
    {

        if (empty($params['room_id'])) {
            Session::push('errors', 'Không xác định thông tin');
            return null;
        }
        $data = array_merge($params, [
            'class_id' => $params['class_id'],
            'room_id' => $params['room_id'],
            'teacher_id' => $params['teacher_id'],
            'day' => $params['day'],
            'mo_ta' => $params['mo_ta'],
            'start_time' => $params['start_time'],
            'end_time' => $params['end_time'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        $res = DB::table($this->table)->insertGetId($data);
        return $res;
    }
    public function saveUpdate($params)
    {

        // dd($params);
        if (empty($params['id'])) {
            Session::push('errors', 'Không xác định thông tin');
            return null;
        }

        $dataUpdate = $params['data'];
        $res = DB::table($this->table)
            ->where('id', $params['id'])
            ->update($dataUpdate);
        return $res;
    }
}
