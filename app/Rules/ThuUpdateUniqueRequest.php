<?php

namespace App\Rules;

use App\Thu;
use Illuminate\Contracts\Validation\Rule;

class ThuUpdateUniqueRequest implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $newName) // hàm này sẽ trả về kiểu boolean(true or false)
    {
        // attribute là name của input truyền vào, newValue là giá trị của input đó
        //name cũ trong db
        $oldName = Thu::find(request()->id)->name;

        //nếu name cũ trong db = nam mới thì cho request tiếp tục
        if($newName === $oldName){
            return true;
        }

        //nếu name mới khác name cũ thì sẽ up date
        // kiểm tra trong db xem có name nào trùng với name vừa nhập hay không
        // nếu có thì trả về false, chưa có thì chả về true
        $kiemTra = Thu::where('name',$newName)->count();
        // dd($kiemTra);
        if($kiemTra>0){
            return false;
        }
        return true;


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Thứ đã tồn tại';
    }
}
