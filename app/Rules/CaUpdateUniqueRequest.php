<?php

namespace App\Rules;

use App\Ca;
use Illuminate\Contracts\Validation\Rule;

class CaUpdateUniqueRequest implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $newName) // hàm này sẽ trả về kiểu boolean(true or false)
    {
        // attribute là name của input truyền vào, newValue là giá trị của input đó
        //name cũ trong db
        $oldName = Ca::find(request()->id)->ca_hoc;

        //nếu name cũ trong db = nam mới thì cho request tiếp tục
        if($newName === $oldName){
            return false;
        }

        //nếu name mới khác name cũ thì sẽ up date
        // kiểm tra trong db xem có name nào trùng với name vừa nhập hay không
        // nếu có thì trả về false, chưa có thì chả về true
        $kiemTra = Ca::where('ca_hoc',$newName)->count();
        if($kiemTra>0){
            return true;
        }
        return true;


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tên ca đã tồn tại';
    }
}
