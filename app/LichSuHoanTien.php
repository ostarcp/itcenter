<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LichSuHoanTien extends Model
{
    protected $fillable = ['id_hoc_vien','id_user','so_tien','ngay_hoan'];

    public function hocVien()
    {
        return $this->hasMany(HocVien::class, 'id', 'id_hoc_vien');
    }

}
