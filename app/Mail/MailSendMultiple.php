<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class MailSendMultiple extends Mailable
{
    use Queueable, SerializesModels;
    protected $lop;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($lop)
    {
        $this->lop = $lop;
    }
 
    public function build()
    {
        return $this
            ->subject('Thông báo quan trọng')
            ->from('phamdoanh225@gmail.com')
            ->view('sendEmailMultiple')
            ->with('lop', $this->lop);;
    }
}