<?php

namespace App\Http\Controllers;

use App\ClassModel;
use App\DangKy;
use App\HocVien;
use App\LichSuHoanTien;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class HoanTienController extends Controller
{
    public function index()
    {
        $listDangKyThuaTienKhiChuyenLop = DangKy::where('du_no', '>', 0)->get();
        // dd($listDangKyThuaTienKhiChuyenLop);
        $listHocVien = HocVien::all();
        // dd();
        // hoàn tiền lại cho sinh viên không nộp nốt học phí khi lớp đó đã khai giảng
        $listClassDaKhaiGiangs = ClassModel::where('start_date', '<',date('Y-m-d'))->get();
        $listDangKyThuaTien = [];
        foreach ($listClassDaKhaiGiangs as $listClassDaKhaiGiang) {
            $dangKiOfListClassDaKhaiGiang = $listClassDaKhaiGiang->dangKy;
            foreach ($dangKiOfListClassDaKhaiGiang as $dangKiOfListClassDaKhaiGiangItem) {
                if ($dangKiOfListClassDaKhaiGiangItem->du_no < 0) {
                    // gán những đăng kí < 0 vào mảng listDangKyThuaTien
                    $listDangKyThuaTien[] = $dangKiOfListClassDaKhaiGiangItem;
                }
            }
        }
        return view('hoanTien.index', compact('listDangKyThuaTienKhiChuyenLop', 'listHocVien', 'listDangKyThuaTien'));
        // return view('hoanTien.index', compact('listDangKyThuaTienKhiChuyenLop', 'listHocVien'));

    }


    //trường hợp hoàn trả học phí cho sinh viên chuyển lớp nhưng thừa tiền
    public function hoanTienDu($id)
    {
        $itemEdit = DangKy::where('id', $id)->first();
        $so_tien_thua = $itemEdit->du_no;
        if ($itemEdit->trang_thai == 1 || $itemEdit->trang_thai == 3) {
            // try {
            //     DB::beginTransaction();
            $payMentEdit = Payment::where('id', $itemEdit->id_payment)->first();
            $soTienHoan = $itemEdit->du_no;
            $payMentEdit['price'] = $payMentEdit->price - $itemEdit->du_no;
            // $payMentEdit->update();
            //sau đó cập nhập bảng dki
            $itemEdit['so_tien_da_dong']  = null;
            $itemEdit['du_no']  = 0;
            //trạng thái = 1 là đăng kí 
            $itemEdit['trang_thai']  = 1;
            $itemEdit->update();
            // DB::commit();

            $itemLichSu = [
                'id_hoc_vien' => $itemEdit->id_hoc_vien,
                'id_user' => $id = auth()->user()->id,
                'so_tien' => $soTienHoan,
                'ngay_hoan' => date('Y-m-d'),

            ];
            LichSuHoanTien::create($itemLichSu);
            $hoc_vien = HocVien::where('id', $itemEdit->id_hoc_vien)->first();
            Mail::send('emailThongBaoHoanTien', compact('hoc_vien', 'so_tien_thua'), function ($email) use ($hoc_vien) {
                $email->subject(" Hệ thống gửi thông báo đã hoàn tiền cho bạn ");
                $email->to($hoc_vien->email, $hoc_vien->name, $hoc_vien);
            });
            return redirect()->back()->with('msg', 'Đã hoàn trả cho sinh viên số tiền thừa');
            // } catch (\Exception $exception) {
            //     DB::rollback();
            //     Log::error('message: ' . $exception->getMessage() . 'line:' . $exception->getLine());
            // }
        }
    }


    //trường hợp hoàn trả học phí cho sinh viên không đóng đủ học phí khi chuyển lớp
    public function edit($id)
    {
        $itemEdit = DangKy::where('id', $id)->first();
        if ($itemEdit->trang_thai == 0) {
            // try {
            //     DB::beginTransaction();
            $payMentEdit = Payment::where('id', $itemEdit->id_payment)->first();
            // treạng thái = 2 là đã hoàn tiền
            $soTienHoan = $payMentEdit->price;

            $payMentEdit['status'] = 2;
            $payMentEdit['description']  = "Hoàn tiền cho sinh viên do không nộp đủ tiền cho lớp học mới ";
            $payMentEdit->update();

            //sau đó cập nhập bảng dki
            $itemEdit['so_tien_da_dong']  = null;
            $itemEdit['du_no']  = 0;
            //trạng thái = 2 là hoàn trả tiền khi khóa học đã bắt dầu
            $itemEdit['trang_thai']  = 0;
            $itemEdit->update();
            // DB::commit();
            $itemLichSu = [
                'id_hoc_vien' => $itemEdit->id_hoc_vien,
                'id_user' => $id = auth()->user()->id,
                'so_tien' => $soTienHoan,
                'ngay_hoan' => date('Y-m-d'),

            ];
            LichSuHoanTien::create($itemLichSu);

            $hoc_vien = HocVien::where('id', $itemEdit->id_hoc_vien)->first();
            // Mail::send('emailHoanTienQuaHan', compact('payMentEdit'), function ($email) use ($hoc_vien) {
            //     $email->subject("Hệ thống gửi thông báo đã hoàn tiền thừa đến bạn");
            //     $email->to($hoc_vien->email, $hoc_vien->name, $hoc_vien);
            // });
            Mail::send('emailHoanTienQuaHan', compact('hoc_vien', 'so_tien_thua','payMentEdit'), function ($email) use ($hoc_vien) {
                $email->subject(" Hệ thống gửi thông báo đã hoàn tiền cho bạn ");
                $email->to($hoc_vien->email, $hoc_vien->name, $hoc_vien);
            });
            return Redirect::back()->withErrors(['msgs' => 'Đã hoàn trả tiền cho sinh viên (chuyển khóa nhưng không nộp đủ học phí)']);
            // } catch (\Exception $exception) {
            //     DB::rollback();
            //     Log::error('message: ' . $exception->getMessage() . 'line:' . $exception->getLine());
            // }
        }
    }

    public function search(Request $request)
    {
        $result = HocVien::where('email', 'LIKE', '%' . $request->email . '%')->get();
        foreach ($result as $resultItem) {
            $dangKyThuaTienOfHocVien = DangKy::where('id_hoc_vien', $resultItem->id)->where('du_no', '>', 0)->get();
        }
        // echo '<pre>';
        //     print($dangKyThuaTienOfHocVien);
        // return redirect('hoanTien')->with(compact('dangKyThuaTienOfHocVien'));
        return redirect()->route('route_BackEnd_list_hoan_tien')->with(['searchs' => $dangKyThuaTienOfHocVien]);
        // return  view('hoanTien.index', compact('dangKyThuaTienOfHocVien'));
    }
}
