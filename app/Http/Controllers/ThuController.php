<?php

namespace App\Http\Controllers;

use App\Http\Requests\ThuRequest\ThuAddRequest;
use App\Http\Requests\ThuRequest\ThuEditRequest;
use App\Thu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ThuController extends Controller
{
    public function index()
    {
        $listThu = Thu::latest()->paginate(5);
        return view('thu.index', compact('listThu'));
    }

    public function add()
    {
        return view('thu.add');
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            // dd($request->all());
            $ts = trim($request->name, "Thứ");
            $tss = (explode(" ", $ts));
            $mangSo = [];
            foreach ($tss as $item) {
                if (is_numeric($item)) {
                    $mangSo[] = $item;
                }
            }
            $key = (implode(",", $mangSo));
            $thuInsert = Thu::create([
                'name' => $request->name,
                'key' => $key
            ]);
            DB::commit();
            session()->flash('success', 'Thêm thành công thứ.');
            return redirect()->route('route_BackEnd_thu_list');
        } catch (\Exception $exception) {
            DB::rollback();
            Log::error('message: ' . $exception->getMessage() . 'line:' . $exception->getLine());
        }
    }

    public function edit($id)
    {
        $thuEdit = Thu::find($id);
        return view('thu.edit', compact('thuEdit'));
    }

    public function update(ThuEditRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $ts = trim($request->name, "Thứ");
            $tss = (explode(" ", $ts));
            $mangSo = [];
            foreach ($tss as $item) {
                if (is_numeric($item)) {
                    $mangSo[] = $item;
                }
            }
            $key = (implode(",", $mangSo));
            $thuUpdate = Thu::find($id)->update([
                'name' => $request->name,
                'key' => $key
            ]);
            DB::commit();
            return redirect()->route('route_BackEnd_thu_list');
        } catch (\Exception $exception) {
            DB::rollback();
            Log::error('message: ' . $exception->getMessage() . 'line:' . $exception->getLine());
        }
    }

    public function delete($id)
    {
        try {
            Thu::find($id)->delete();
            return response()->json([
                'code' => 200,
                'message' => 'success'
            ]);
        } catch (\Exception $exception) {
            Log::error('Message:' . $exception->getMessage() . '---Line: ' . $exception->getLine());
            return response()->json([
                'code' => 500,
                'message' => 'fail',
            ]);
        }
    }
}
