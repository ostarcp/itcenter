<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseCategory;
use App\CentralFacility;
use App\ClassModel;
use App\ClassRoom;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ClassRoomRequest;
use Illuminate\Support\Facades\Session;
use Spipu\Html2Pdf\Html2Pdf;
use Dompdf\Dompdf;
use Dompdf\Options;

require_once __DIR__ . '/../../SLib/functions.php';

class ClassRoomController extends Controller
{
    private $v;

    public function __construct()
    {
        $this->v = [];
    }

    public function classRoom(Request $request)
    {
        $this->v['_title'] = 'Phòng Học';
        $this->v['routeIndexText'] = 'Phòng Học';
        $objClassRoom = new ClassRoom();
        //Nhận dữ liệu lọc từ view
        $this->v['extParams'] = $request->all();
        // dd($request->all());
        $this->v['list'] = $objClassRoom->loadListWithPager($this->v['extParams']);
        $objCentralFacility = new CentralFacility();
        $this->v['central_facility'] = $objCentralFacility->loadListIdAndName();
        $centralFacility = $this->v['central_facility'];
        $arrCentralFacility = [];
        foreach ($centralFacility as $index => $item) {
            $arrCentralFacility[$item->id] = $item->name;
        }
        $this->v['arrCentralFacility'] = $arrCentralFacility;
        return view('phonghoc.class_room', $this->v);
    }

    public function AddClassRoom(ClassRoomRequest $request)
    {
        $this->v['_title'] = 'Phòng Học';
        $method_route = 'route_BackEnd_ClassRoom_Add';
        $this->v['_action'] = 'Add';
        $this->v['_title'] = 'Thêm phòng học';
        $this->v['trang_thai'] = config('app.status_user');
        // dd(123);
        if ($request->isMethod('post')) {
            // dd(456);
            if (Session::has($method_route)) {
                return redirect()->route($method_route); // không cho F5, chỉ có thể post 1 lần
            } else
                Session::push($method_route, 1); // bỏ vào session để chống F5

            $params = [
                'user_add' => Auth::user()->id
            ];
            $params['cols'] = array_map(function ($item) {
                if ($item == '')
                    $item = null;
                if (is_string($item))
                    $item = trim($item);
                return $item;
            }, $request->post());

            // dd($params['cols']);
            unset($params['cols']['_token']);
            $objClassRoom = new ClassRoom();
            $res = $objClassRoom->saveNew($params);
            // dd($res);
            if ($res == null) // chuyển trang vì trong session đã có sẵn câu thông báo lỗi rồi
            {
                Session::push('post_form_data', $this->v['request']);
                return redirect()->route($method_route);
            } elseif ($res > 0) {
                $this->v['request'] = [];
                $request->session()->forget('post_form_data'); // xóa data post
                Session::flash('success', 'Thêm mới thành công Phòng học !');
                return redirect()->route('route_BackEnd_ClassRoom_List');

            } else {
                Session::push('errors', 'Lỗi thêm mới: ' . $res);
                Session::push('post_form_data', $this->v['request']);
                return redirect()->route($method_route);
            }

        } else {
            // không phải post
            $request->session()->forget($method_route); // hủy session nếu vào bằng sự kiện get
        }

        $objCentralFacility= new  CentralFacility();
        $this->v['centralFacility'] = $objCentralFacility->loadListIdAndName();
        return view('phonghoc.class_room_add', $this->v);

    }

    public function classRoomDetail($id, Request $request){
        $this->v['routeIndexText'] = 'Chi tiết phòng học';
        $this->v['_action'] = 'Edit';
        $this->v['_title'] = 'Chi tiết phòng học';
        $objClassRoom = new ClassRoom();
        $objItem = $objClassRoom->loadOne($id);
        $this->v['classRoom_id'] = $objClassRoom->loadListIdAndName();
        $this->v['objItem'] = $objItem;
        $classRoom = $this->v['classRoom_id'];
        // dd($user);
        $arrClassRoom = [];
        foreach ($classRoom as $index => $item) {
            // dd($item);
            $arrClassRoom[$item->id] = $item->name;
            
        }
        $this->v['arrClassRoom'] = $arrClassRoom;
        if (empty($objItem)) {
            Session::push('errors', 'Không tồn tại phòng học này ' . $id);
            return redirect()->back();
        }
        $this->v['extParams'] = $request->all();

        
       
        $objCentralFacility = new CentralFacility();
        $this->v['centralFacility'] = $objCentralFacility->loadListIdAndName();
        $centralFacility = $this->v['centralFacility'];
        // dd($user);
        $arrFacility = [];
        foreach ($centralFacility as $index => $item) {
            // dd($item);
            $arrFacility[$item->id] = $item->name;
            
        }
        $this->v['arrFacility'] = $arrFacility;
        
        return view('phonghoc.class_room_detail',$this->v);
    }


    public function updateClassRoom($id, ClassRoomRequest $request){

        $method_route = 'route_BackEnd_ClassRoom_Detail';
        $modelClassRoom = new ClassRoom();
        $params = [
            'user_edit' => Auth::user()->id
        ];
        $params['cols'] = array_map(function ($item) {
            if($item == '')
                $item = null;
            if(is_string($item))
                $item = trim($item);
            return $item;
        }, $request->post());
        
        unset($params['cols']['_token']);

        $objItem = $modelClassRoom->loadOne($id);
        if (empty($objItem)) {
            Session::push('errors', 'Không tồn tại người dùng này ' . $id);
            return redirect()->route('route_BackEnd_NguoiDung_index');
        }
        $params['cols']['id'] = $id;
        // dd($params);
        $res = $modelClassRoom->saveUpdate($params);

        if ($res == null) // chuyển trang vì trong session đã có sẵn câu thông báo lỗi rồi
        {
            Session::push('post_form_data', isset($this->v['request'])?$this->v['request'] : '');
            return redirect()->route($method_route, ['id' => $id]);
        } elseif ($res == 1) {
//            SpxLogUserActivity(Auth::user()->id, 'edit', $primary_table, $id, 'edit');
            $request->session()->forget('post_form_data'); // xóa data post
            Session::flash('success', 'Cập nhật thành công thông tin phòng học!');
            return redirect()->route('route_BackEnd_ClassRoom_List');
        } else {

            Session::push('errors', 'Lỗi cập nhật cho bản ghi: ' . $res);
            Session::push('post_form_data', $this->v['request']);
            return redirect()->route($method_route, ['id' => $id]);
        }
    }

    public function destroy($id)
{
	//Xoa hoc sinh
	//Thực hiện câu lệnh xóa với giá trị id = $id trả về
	$deleteData = DB::table('class_rooms')->where('id', '=', $id)->delete();
	
	//Kiểm tra lệnh delete để trả về một thông báo
	if ($deleteData) {
		Session::flash('success', 'Xóa phòng học thành công!');
	}else {                        
		Session::flash('error', 'Xóa thất bại!');
	}
	
	//Thực hiện chuyển trang
	return redirect()->route('route_BackEnd_ClassRoom_List');
}
    
}
//