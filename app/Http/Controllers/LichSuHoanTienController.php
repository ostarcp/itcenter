<?php

namespace App\Http\Controllers;

use App\HocVien;
use App\LichSuHoanTien;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;

class LichSuHoanTienController extends Controller
{
    public function index(){
        $listLichSu = LichSuHoanTien::latest()->paginate(5);
        // dd($listLichSu);
        $listHocVien = HocVien::all();
        $listUser = User::all();
        return view('lichSuHoanTien.index',compact('listLichSu','listHocVien','listUser'));
    }
}
