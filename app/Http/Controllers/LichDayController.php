<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\XepPhong;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LichDayController extends Controller
{
    public function index()
    {

        $this->v['_title'] = 'Lịch dạy của tôi';
        $lichDay = 'đang chờ';

        $objXepPhong = new XepPhong();
        $teacher_id = optional(Teacher::where('user_id', Auth::id())->first())->id;
        $this->v['lich_day'] = $objXepPhong->loadOneTeacher($teacher_id);
        return view('lichDay.index', $this->v, compact('lichDay'));
    }
}
