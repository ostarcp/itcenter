<?php

namespace App\Http\Controllers;

use App\ClassModel;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\DanhMucKhoaHocRequest;
use App\Http\Requests\CourseCateGoryRequest;
// use Illuminate\Support\Facades\App\CourseCategory;
use App\CourseCategory;
use App\Course;
use App\HocVien;
use App\Payment;
use App\Teacher;
use App\Thongke;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spipu\Html2Pdf\Html2Pdf;
use Dompdf\Dompdf;
use Carbon\Carbon;
use Dompdf\Options;


class ThongKeController extends Controller
{
    private $v;

    public function __construct()
    {
        $this->v = [];
    }
    public function thongKeCung(Request $request)
    {
        //thống kê số lớp active
        $now = date('Y-m-d');
        $objClass = new ClassModel();
        $objCourse = new Course();
        $objHS = new HocVien();
        $objTeacher = new Teacher();
        $objPayment = new Payment();
        // dd($request->search_ngay_khai_giang);
        //số khóa học đang hoạt động
        $activeCourse = $objCourse->loadListIdAndName(['status', 1])->count();
        $this->v['khoahoc_danghoatdong'] = $activeCourse;
        // số lớp đang học
        $lopdanghoc = $objClass->loadActiveClass()->count();
        $this->v['lop_dang_hoc'] = $lopdanghoc;
        //tổng số lớp
        $lophoc = $objClass->loadListIdAndName();
        $this->v['lop_hoc'] = $lophoc->count();
        //tổng số học sinh
        $activeHS = $objHS->loadCountHV();
        $this->v['tong_so_hoc_vien'] = $activeHS;
        //số học viên đang học:
        $hvdanghoc = $objClass->loadActiveHvien()->count();
        $this->v['hoc_vien_dang_hoc'] = $hvdanghoc;
        //tổng số giảng viên
        $teacher = $objTeacher->loadActive()->count();
        $this->v['tong_so_giang_vien'] = $teacher;
        //số giảng viên đang có lớp
        $teacherInClass = $objTeacher->loadInClass()->count();
        // dd($teacherInClass);
        $this->v['so_giang_vien_dang_trong_lop'] = $teacherInClass;
        // dd($teacherInClass);
        //tổng học phí
        $hoc_phi = $objPayment->sumAllPay();
        $this->v['hoc_phi'] = number_format($hoc_phi);
        // dd($hoc_phi);
        //tổng học phí đã thu
        $tong_hoc_phi = $objPayment->sumPay();
        $this->v['tong_hoc_phi'] = number_format($tong_hoc_phi);
        $course_name = $objCourse->loadListIdAndName();
        $this->v['course_name'] = $course_name;
        $loadmax = $objPayment->loadMaxCourseCung();


        $max = 0;
        for ($i = 0; $i < count($loadmax); $i++) {
            if ($loadmax[$i]->sum > $max) {
                $max = $loadmax[$i]->sum;
                $name = $loadmax[$i]->name;
            };
        }
        // dd($max, $name);
        $this->v['khoa_nhieu_nhat_cung'] = number_format($max);
        $this->v['ten_khoa_nhieu_cung'] = $name;

        // *************************************************************************************************************************
        //THỐNG KÊ MỀM
        if (isset($request->search_ngay)) {


            $input = $request->search_ngay;
            $explo = explode(
                ' - ',
                $input
            );
            $time[0] = Carbon::createFromFormat('d/m/Y', $explo[0])->format('Y/m/d');
            $time[1] = Carbon::createFromFormat('d/m/Y', $explo[1])->format('Y/m/d');
            //số học phí đã thu
            $a = $objPayment->loadpayDay($time);
            $this->v['so_hoc_phi'] = number_format($a);
            //tổng học phí
            $timehocphi = $objPayment->loadAllPayDay($time);
            $this->v['so_hoc_phi_all'] = number_format($timehocphi);
            //giảng viên đã dạy trong quãng thời gian
            $b = $objTeacher->loadDay($time);
            // số học sinh đã đăng kí lớp trong tgian đó
            $c = $objPayment->loadstd($time)->count();
            $this->v['hs_dk_moi'] = $c;

            $this->v['time'] = $request->search_ngay;
            $batdau = Carbon::createFromFormat('Y/m/d', $time[0]);
            $ketthuc = Carbon::createFromFormat('Y/m/d', $time[1]);
            $i = -1;
            $loparr = [];
            foreach ($b as $key => $item) {
                $x = $item->start_date;
                $y = $item->end_date;
                $lopBD = Carbon::createFromFormat('Y-m-d', $x);
                $lopKT = Carbon::createFromFormat('Y-m-d', $y);
                if ($batdau->gt($lopBD) && $lopKT->gt($batdau)) {
                    $i++;
                    $loparr[$i] = $item;
                } elseif ($lopBD->gt($batdau) && $ketthuc->gt($lopBD)) {
                    $i++;
                    $loparr[$i] = $item;
                };
            }
            $loadmax = $objPayment->loadMaxCourse($time);


            $max = 0;
            for ($i = 0; $i < count($loadmax); $i++) {
                if ($loadmax[$i]->sum > $max) {
                    $max = $loadmax[$i]->sum;
                    $name = $loadmax[$i]->name;
                };
            }
            // dd($max, $name);
            $this->v['khoa_nhieu_nhat'] = number_format($max);
            $this->v['ten_khoa_nhieu'] = $name;
            // echo ('<pre>');
            // var_dump($loparr);
        }
        if (isset($request->search_ngay) && isset($request->khoa_hoc)) {
            $input = $request->search_ngay;
            $explo = explode(
                ' - ',
                $input
            );
            $time[0] = Carbon::createFromFormat('d/m/Y', $explo[0])->format('Y/m/d');
            $time[1] = Carbon::createFromFormat('d/m/Y', $explo[1])->format('Y/m/d');
            $a = $objPayment->loadpayDay($time);
            $this->v['so_hoc_phi'] = number_format($a);
            //tổng học phí
            $timehocphi = $objPayment->loadAllPayDay($time);
            $this->v['so_hoc_phi_all'] = number_format($timehocphi);
            //giảng viên đã dạy trong quãng thời gian
            $b = $objTeacher->loadDay($time);
            // số học sinh đã đăng kí lớp trong tgian đó
            $c = $objPayment->loadstd($time)->count();
            $this->v['hs_dk_moi'] = $c;
            $this->v['time'] = $request->search_ngay;
            $course = $objCourse->loadOneID($request->khoa_hoc);
            $this->v['course'] = $course;
            $batdau = Carbon::createFromFormat('Y/m/d', $time[0]);
            $ketthuc = Carbon::createFromFormat('Y/m/d', $time[1]);
            $i = -1;
            $loparr = [];
            foreach ($b as $key => $item) {
                $x = $item->start_date;
                $y = $item->end_date;
                $lopBD = Carbon::createFromFormat('Y-m-d', $x);
                $lopKT = Carbon::createFromFormat('Y-m-d', $y);
                if ($batdau->gt($lopBD) && $lopKT->gt($batdau)) {
                    $i++;
                    $loparr[$i] = $item;
                } elseif ($lopBD->gt($batdau) && $ketthuc->gt($lopBD)) {
                    $i++;
                    $loparr[$i] = $item;
                };
            }
            $course_id = $request->khoa_hoc;
            $loadamount = $objPayment->loadPayAmount($time, $course_id);
            $loadmax = $objPayment->loadMaxCourse($time);


            $max = 0;
            for ($i = 0; $i < count($loadmax); $i++) {
                if ($loadmax[$i]->sum > $max) {
                    $max = $loadmax[$i]->sum;
                    $name = $loadmax[$i]->name;
                };
            }
            // dd($max, $name);
            // $this->v['khoa_nhieu_nhat'] = number_format($max);
            // $this->v['ten_khoa_nhieu'] = $name;
            // dd($time, $now);
            // dd($loadamount);
            //tổng số tiền của khóa theo thời gian
            $this->v['tong_so_tien_khoa'] = number_format($loadamount);
            //số học sinh đăng kí mới khóa học
            $loadstudent = $objPayment->loadStudent($time, $course_id);
            // dd($loadstudent);
            $this->v['tong_so_hoc_vien_moi'] = $loadstudent;
        }
        return view('thongke', $this->v);
    }
    public function thongKeTS(Request $request)
    {
        $now = date('Y-m-d');
        $objClass = new ClassModel();
        $objCourse = new Course();
        $objHS = new HocVien();
        $objTeacher = new Teacher();
        $objPayment = new Payment();
        //số khóa học đang hoạt động
        $activeCourse = $objCourse->loadListIdAndName(['status', 1])->count();
        $this->v['khoahoc_danghoatdong'] = $activeCourse;
        // số lớp đang học
        $lopdanghoc = $objClass->loadActiveClass()->count();
        $this->v['lop_dang_hoc'] = $lopdanghoc;
        //tổng số lớp
        $lophoc = $objClass->loadListIdAndName();
        $this->v['lop_hoc'] = $lophoc->count();
        //tổng số học sinh
        $activeHS = $objHS->loadCountHV();
        $this->v['tong_so_hoc_vien'] = $activeHS;
        //số học viên đang học:
        $hvdanghoc = $objClass->loadActiveHvien()->count();
        $this->v['hoc_vien_dang_hoc'] = $hvdanghoc;
        //THỐNG KÊ MỀM
        if (isset($request->search_ngay)) {


            $input = $request->search_ngay;
            $explo = explode(
                ' - ',
                $input
            );
            $time[0] = Carbon::createFromFormat('d/m/Y', $explo[0])->format('Y/m/d');
            $time[1] = Carbon::createFromFormat('d/m/Y', $explo[1])->format('Y/m/d');
            //số học phí đã thu
            $a = $objPayment->loadpayDay($time);
            $this->v['so_hoc_phi'] = number_format($a);
            //tổng học phí
            $timehocphi = $objPayment->loadAllPayDay($time);
            $this->v['so_hoc_phi_all'] = number_format($timehocphi);
            //giảng viên đã dạy trong quãng thời gian
            $b = $objTeacher->loadDay($time);
            // số học sinh đã đăng kí lớp trong tgian đó
            $c = $objPayment->loadstd($time)->count();
            $this->v['hs_dk_moi'] = $c;

            $this->v['time'] = $request->search_ngay;
            $batdau = Carbon::createFromFormat('Y/m/d', $time[0]);
            $ketthuc = Carbon::createFromFormat('Y/m/d', $time[1]);
            $i = -1;
            $loparr = [];
            foreach ($b as $key => $item) {
                $x = $item->start_date;
                $y = $item->end_date;
                $lopBD = Carbon::createFromFormat('Y-m-d', $x);
                $lopKT = Carbon::createFromFormat('Y-m-d', $y);
                if ($batdau->gt($lopBD) && $lopKT->gt($batdau)) {
                    $i++;
                    $loparr[$i] = $item;
                } elseif ($lopBD->gt($batdau) && $ketthuc->gt($lopBD)) {
                    $i++;
                    $loparr[$i] = $item;
                };
            }
        }
        return view('thongkeTS', $this->v);
    }
    public function thongKeDT(Request $request)
    {
        $now = date('Y-m-d');
        $objClass = new ClassModel();
        $objCourse = new Course();
        $objHS = new HocVien();
        $objTeacher = new Teacher();
        $objPayment = new Payment();
        $teacherInClass = $objTeacher->loadInClass()->count();
        // dd($teacherInClass);
        $this->v['so_giang_vien_dang_trong_lop'] = $teacherInClass;
        //tổng số học sinh
        $activeHS = $objHS->loadCountHV();
        $this->v['tong_so_hoc_vien'] = $activeHS;
        //tổng số lớp
        $lophoc = $objClass->loadListIdAndName();
        $this->v['lop_hoc'] = $lophoc->count();
        //số khóa học đang hoạt động
        $activeCourse = $objCourse->loadListIdAndName(['status', 1])->count();
        $this->v['khoahoc_danghoatdong'] = $activeCourse;
        // số lớp đang học
        $lopdanghoc = $objClass->loadActiveClass()->count();
        $this->v['lop_dang_hoc'] = $lopdanghoc;
        //tổng số giảng viên
        $teacher = $objTeacher->loadActive()->count();
        $this->v['tong_so_giang_vien'] = $teacher;
        //số học viên đang học:
        $hvdanghoc = $objClass->loadActiveHvien()->count();
        $this->v['hoc_vien_dang_hoc'] = $hvdanghoc;
        //THỐNG KÊ MỀM
        if (isset($request->search_ngay)) {


            $input = $request->search_ngay;
            $explo = explode(
                ' - ',
                $input
            );
            $time[0] = Carbon::createFromFormat('d/m/Y', $explo[0])->format('Y/m/d');
            $time[1] = Carbon::createFromFormat('d/m/Y', $explo[1])->format('Y/m/d');
            //số học phí đã thu
            $a = $objPayment->loadpayDay($time);
            $this->v['so_hoc_phi'] = number_format($a);
            //tổng học phí
            $timehocphi = $objPayment->loadAllPayDay($time);
            $this->v['so_hoc_phi_all'] = number_format($timehocphi);
            //giảng viên đã dạy trong quãng thời gian
            $b = $objTeacher->loadDay($time);
            // số học sinh đã đăng kí lớp trong tgian đó
            $c = $objPayment->loadstd($time)->count();
            $this->v['hs_dk_moi'] = $c;

            $this->v['time'] = $request->search_ngay;
            $batdau = Carbon::createFromFormat('Y/m/d', $time[0]);
            $ketthuc = Carbon::createFromFormat('Y/m/d', $time[1]);
            $i = -1;
            $loparr = [];
            foreach ($b as $key => $item) {
                $x = $item->start_date;
                $y = $item->end_date;
                $lopBD = Carbon::createFromFormat('Y-m-d', $x);
                $lopKT = Carbon::createFromFormat('Y-m-d', $y);
                if ($batdau->gt($lopBD) && $lopKT->gt($batdau)) {
                    $i++;
                    $loparr[$i] = $item;
                } elseif ($lopBD->gt($batdau) && $ketthuc->gt($lopBD)) {
                    $i++;
                    $loparr[$i] = $item;
                };
            }
        }
        return view('thongkeDT', $this->v);
    }
    public function thongKeKT(Request $request)
    {
        $now = date('Y-m-d');
        $objClass = new ClassModel();
        $objCourse = new Course();
        $objHS = new HocVien();
        $objTeacher = new Teacher();
        $objPayment = new Payment();
        //tổng số học sinh
        $activeHS = $objHS->loadCountHV();
        $this->v['tong_so_hoc_vien'] = $activeHS;
        //số học viên đang học:
        $hvdanghoc = $objClass->loadActiveHvien()->count();
        $this->v['hoc_vien_dang_hoc'] = $hvdanghoc;
        $hoc_phi = $objPayment->sumAllPay();
        $this->v['hoc_phi'] = number_format($hoc_phi);
        // dd($hoc_phi);
        //tổng học phí đã thu
        $tong_hoc_phi = $objPayment->sumPay();
        $this->v['tong_hoc_phi'] = number_format($tong_hoc_phi);
        $course_name = $objCourse->loadListIdAndName();
        $this->v['course_name'] = $course_name;
        // dd();
        //THỐNG KÊ MỀM
        if (isset($request->search_ngay) && empty($request->khoa_hoc)) {


            $input = $request->search_ngay;
            $explo = explode(
                ' - ',
                $input
            );
            $time[0] = Carbon::createFromFormat('d/m/Y', $explo[0])->format('Y/m/d');
            $time[1] = Carbon::createFromFormat('d/m/Y', $explo[1])->format('Y/m/d');
            //số học phí đã thu
            $a = $objPayment->loadpayDay($time);
            $this->v['so_hoc_phi'] = number_format($a);
            //tổng học phí
            $timehocphi = $objPayment->loadAllPayDay($time);
            $this->v['so_hoc_phi_all'] = number_format($timehocphi);
            //giảng viên đã dạy trong quãng thời gian
            $b = $objTeacher->loadDay($time);
            // số học sinh đã đăng kí lớp trong tgian đó
            $c = $objPayment->loadstd($time)->count();
            $this->v['hs_dk_moi'] = $c;
            $this->v['time'] = $request->search_ngay;
            $batdau = Carbon::createFromFormat('Y/m/d', $time[0]);
            $ketthuc = Carbon::createFromFormat('Y/m/d', $time[1]);
            $i = -1;
            $loparr = [];
            foreach ($b as $key => $item) {
                $x = $item->start_date;
                $y = $item->end_date;
                $lopBD = Carbon::createFromFormat('Y-m-d', $x);
                $lopKT = Carbon::createFromFormat('Y-m-d', $y);
                if ($batdau->gt($lopBD) && $lopKT->gt($batdau)) {
                    $i++;
                    $loparr[$i] = $item;
                } elseif ($lopBD->gt($batdau) && $ketthuc->gt($lopBD)) {
                    $i++;
                    $loparr[$i] = $item;
                };
            }
        }
        if (isset($request->search_ngay) && isset($request->khoa_hoc)) {
            $input = $request->search_ngay;
            $explo = explode(
                ' - ',
                $input
            );
            $time[0] = Carbon::createFromFormat('d/m/Y', $explo[0])->format('Y/m/d');
            $time[1] = Carbon::createFromFormat('d/m/Y', $explo[1])->format('Y/m/d');
            $a = $objPayment->loadpayDay($time);
            $this->v['so_hoc_phi'] = number_format($a);
            //tổng học phí
            $timehocphi = $objPayment->loadAllPayDay($time);
            $this->v['so_hoc_phi_all'] = number_format($timehocphi);
            //giảng viên đã dạy trong quãng thời gian
            $b = $objTeacher->loadDay($time);
            // số học sinh đã đăng kí lớp trong tgian đó
            $c = $objPayment->loadstd($time)->count();
            $this->v['hs_dk_moi'] = $c;
            $this->v['time'] = $request->search_ngay;
            $course = $objCourse->loadOneID($request->khoa_hoc);
            $this->v['course'] = $course;
            $batdau = Carbon::createFromFormat('Y/m/d', $time[0]);
            $ketthuc = Carbon::createFromFormat('Y/m/d', $time[1]);
            $i = -1;
            $loparr = [];
            foreach ($b as $key => $item) {
                $x = $item->start_date;
                $y = $item->end_date;
                $lopBD = Carbon::createFromFormat('Y-m-d', $x);
                $lopKT = Carbon::createFromFormat('Y-m-d', $y);
                if ($batdau->gt($lopBD) && $lopKT->gt($batdau)) {
                    $i++;
                    $loparr[$i] = $item;
                } elseif ($lopBD->gt($batdau) && $ketthuc->gt($lopBD)) {
                    $i++;
                    $loparr[$i] = $item;
                };
            }
            $course_id = $request->khoa_hoc;
            $loadamount = $objPayment->loadPayAmount($time, $course_id);
            // dd($loadamount);
            //tổng số tiền của khóa theo thời gian
            $this->v['tong_so_tien_khoa'] = number_format($loadamount);
            //số học sinh đăng kí mới khóa học
            $loadstudent = $objPayment->loadStudent($time, $course_id);
            // dd($loadstudent);
            $this->v['tong_so_hoc_vien_moi'] = $loadstudent;
        }
        return view('thongkeKT', $this->v);
    }
}
