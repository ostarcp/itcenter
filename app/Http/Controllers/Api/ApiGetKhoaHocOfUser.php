<?php

namespace App\Http\Controllers\Api;

use App\ClassModel;
use App\XepPhong;
use App\Ca;
use App\ClassRoom;
use App\Course;
use App\CourseCategory;
use App\DangKy;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Middleware\checkTokenSend;
use App\SessionUser;
use App\Teacher;

class ApiGetKhoaHocOfUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tokenUp = $request->bearerToken();
        $id_user = SessionUser::where('token', $tokenUp)->first()->user_id;
        $listDangKiOfUser = DangKy::where('id_hoc_vien', $id_user)->get()->toArray();
        $data = [];
        if ($listDangKiOfUser) {
            foreach ($listDangKiOfUser as $listDangKiOfUserItem) {
                $listDangKiOfUserItem['lop_hoc'] = ClassModel::where('id', $listDangKiOfUserItem['id_lop_hoc'])->first();
                $course_data = Course::find($listDangKiOfUserItem['lop_hoc']->course_id);
                $listDangKiOfUserItem['lop_hoc']->image = $course_data->image;
                // $listDangKiOfUserItem['thong_tin_phong'] = XepPhong::where('class_id', $listDangKiOfUserItem['lop_hoc']->id)->paginate(10);
                array_push($data, $listDangKiOfUserItem);
            }
            return response()->json([
                'status' => true,
                'heading' => "Danh sách khóa học đăng kí",
                'data' => $data,
            ], 200);
        }
        return response()->json([
            'status' => true,
            'heading' => "Bạn chƯa đang kí lớp học nào",
        ], 200);
        // }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTimeTableOfStudent(Request $request)
    {
        //
        $tokenUp = $request->bearerToken();
        $id_user = SessionUser::where('token', $tokenUp)->first()->user_id;
        $listDangKiOfUser = DangKy::where('id_hoc_vien', $id_user)->get()->toArray();

        $data = [];
        if ($listDangKiOfUser) {
            foreach ($listDangKiOfUser as $listDangKiOfUserItem) {
                $listCaHoc = optional(XepPhong::where('class_id', $listDangKiOfUserItem['id_lop_hoc'])->paginate(50))->toArray();

                $dataListCaHoc = [];
                foreach ($listCaHoc['data'] as $caHocitem) {
                    $caHocitem['room_name'] = optional(ClassRoom::find($caHocitem['room_id']))->name;
                    $caHocitem['class_name'] = optional(ClassModel::find($caHocitem['class_id']))->name;
                    $caHocitem['teacher_name'] = optional(Teacher::find($caHocitem['teacher_id']))->name;
                    array_push($dataListCaHoc, $caHocitem);
                }

                $listDangKiOfUserItem['list_ca_hoc'] = $dataListCaHoc;

                array_push($data, $listDangKiOfUserItem);
            }
            return response()->json([
                'status' => true,
                'heading' => "Danh sách khóa học đăng kí",
                'data' => $data,
            ], 200);
        }
        return response()->json([
            'status' => true,
            'heading' => "Bạn chƯa đang kí lớp học nào",
        ], 200);
    }
}
