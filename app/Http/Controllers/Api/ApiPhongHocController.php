<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ClassRoom;

use Illuminate\Http\Request;

class ApiPhongHocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $coSo = new ClassRoom();
        $dataRaw = $coSo->loadListWithPager($request->all())->toArray();
        // dd($request->all());

        return response()->json([
            'status' => 200,
            'data' => $dataRaw['data'],
            'total' => $dataRaw['total']
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ClassRoom::find($id)->first();
        return response()->json([
            'status' => true,
            'heading' => "Thông tin chi tiết",
            'data' => $data
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
