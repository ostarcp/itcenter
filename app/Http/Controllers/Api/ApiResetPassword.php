<?php

namespace App\Http\Controllers\Api;

use App\HocVien;
use App\Http\Controllers\Controller;
use App\SessionUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;



class ApiResetPassword extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->email;
        if ($request->email) {
            $checkIssetEmail = HocVien::where('email', $email)->first();
            if (isset($checkIssetEmail) && $checkIssetEmail) {
                $checkIssetEmail['token_reset_password'] = Str::random(20);
                $checkIssetEmail['token_expire_date'] = date('Y-m-d H:i:s', strtotime('+1 day'));
                $checkIssetEmail->update();
                // $link = $checkIssetEmail->email.$checkIssetEmail->token_reset_password;
                // dd($link);
                Mail::send('emailResetPassword', compact('checkIssetEmail'), function ($email) use ($checkIssetEmail) {
                    $email->subject(" Hệ thống gửi thông tin reset password ");
                    $email->to($checkIssetEmail->email, $checkIssetEmail->name, $checkIssetEmail);
                });

                $checkIssetToken = SessionUser::where('user_id', $checkIssetEmail->id)->first();
                if ($checkIssetToken) {
                    //xoa sesion token di
                    $checkIssetToken->delete();
                }
                return response()->json([
                    'data' => $checkIssetEmail,
                    'heading' => 'Tồn tại email',
                    'status' => true,
                ], 200);
            }
            return response()->json([
                'heading' => 'Email không tồn tại trên hệ thống',
                'status' => false,
            ], 200);
        }
    }

    public function getThongTin(Request $request)
    {
        //check thời gian sống của token
        $userEmail = HocVien::where('email', $request->email)->where('token_reset_password', $request->token)->first();
        if ($userEmail) {
            if (strtotime($userEmail->token_expire_date) < strtotime(date("Y-m-d H:i:s"))) {
                return response()->json([
                    'status' => false,
                    'heading' => "Hết thời gian để ấn link, vui lòng thực hiện lại"
                ], 500);
            } else {
                return response()->json([
                    'status' => true,
                    'heading' => "Chuyền sang trang đăng kí đổi mật khẩu",
                    'data' => 'http://127.0.0.1:8000/api/getThongTin?email=doanhptph10742@fpt.edu.vn&token=uyruytiyopi3n3ki3'
                ], 200);
            }
        }
        return response()->json([
            'status' => false,
            'heading' => "Token hoặc email chưa đúng"
        ], 500);
    }

    public function hashPass(Request $request)
    {
        // dd($request->all());
        // $decrypt= Crypt::decryptString($encrypted);
        // dd($decrypt);
        if (isset($request->email) && $request->email && isset($request->password) && $request->password) {
            $emailUpdatePass = HocVien::where('email', $request->email)->first();
            $passHash = Crypt::encryptString($request->password);
            $emailUpdatePass['password'] = $passHash;
            $emailUpdatePass->update();
            return response()->json([
                'status' => true,
                'heading' => "Đổi mật khẩu thành công",
                'data' => 'cho link đang nhập vào đây'
            ], 200);
        }
        return response()->json([
            'status' => false,
            'heading' => "Không gửi email hoặc password"
        ], 500);
       
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
