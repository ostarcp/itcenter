<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\XepPhong;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Course;

class ApiXepPhongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $rules = [
            'class_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'heading' => 'validation error',
                'error' => $validator->errors()
            ], 400);
        }

        $xepPhong = new XepPhong();
        $dataRaw = optional($xepPhong->loadListWithPager($request->all()))->toArray();


        foreach ($dataRaw['data'] as $item) {
            $course_id = optional(XepPhong::where('class_id', '=', $item->class_id)->first()->class)->course_id;

            $item->course = Course::find($course_id)->name;
            $item->room = optional(XepPhong::where('room_id', '=', $item->room_id)->first()->room)->name;
            $item->class = optional(XepPhong::where('room_id', '=', $item->room_id)->first()->class)->name;
            $item->teacher = optional(XepPhong::where('teacher_id', '=', $item->teacher_id)->first()->teacher)->name;
        }

        return response()->json([
            'status' => 200,
            'data' => $dataRaw['data'],
            'total' => $dataRaw['total']
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $xepPhong = new XepPhong();
        // dd($request->all());
        $rules = [
            'class_id' => 'required',
            'room_id' => 'required',
            'teacher_id' => 'required',
            'day' => 'required',
            // 'day' => 'required|in:Thứ 2,Thứ 3,Thứ 4,Thứ 5,Thứ 6,Thứ 7,Chủ nhật',
            'start_time' => 'required',
            'end_time' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'heading' => 'Chưa qua được validate',
                'error' => $validator->errors()
            ], 400);
        }


        $xepPhongObj = new XepPhong();

        $start_time = $request->start_time;
        $end_time = $request->end_time;

        if ($start_time == $end_time || $start_time > $end_time) {
            return response()->json([
                'status' => 400,
                'heading' => 'Thời gian không hợp lệ',
                'error' => "Time err"
            ], 400);
        }



        $checkRoom = $xepPhongObj->checkExistRoom($request->room_id, $request->day, $request->start_time, $request->end_time);
        $checkTeacher = $xepPhongObj->checkExistTeacher($request->teacher_id, $request->day, $request->start_time, $request->end_time);

        if ($checkRoom) {
            return response()->json([
                'status' => 500,
                'heading' => 'Phòng đã được xếp lịch',
                'error' => $checkRoom
            ], 500);
        }

        if ($checkTeacher) {
            return response()->json([
                'status' => 500,
                'heading' => 'Giáo viên đã được xếp lịch',
                'error' => $checkTeacher
            ], 500);
        }


        $body = [
            'class_id' => $request->class_id,
            'room_id' => $request->room_id,
            'teacher_id' => $request->teacher_id,
            'day' => $request->day,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'mo_ta' => $request->mo_ta,
        ];

        $addTimeTable = $xepPhongObj->saveNew($body);

        return response()->json([
            'status' => 200,
            'heading' => 'Thêm mới thành công',
            'data' => $addTimeTable
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = XepPhong::find($id)->first();
        return response()->json([
            'status' => true,
            'heading' => "Thông tin chi tiết",
            'data' => $data
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'class_id' => 'required',
            'room_id' => 'required',
            'teacher_id' => 'required',
            'day' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'heading' => 'Chưa qua được validate',
                'error' => $validator->errors()
            ], 400);
        }


        $xepPhongObj = new XepPhong();

        $start_time = $request->start_time;
        $end_time = $request->end_time;

        if ($start_time == $end_time || $start_time > $end_time) {
            return response()->json([
                'status' => 400,
                'heading' => 'Thời gian không hợp lệ',
                'error' => "Time err"
            ], 400);
        }



        $record = $xepPhongObj->checkExistUpdate($request->room_id, $request->day, $request->start_time, $request->end_time);
        $checkRoom = $xepPhongObj->checkUpdateExistRoom($request->id, $request->room_id, $request->day, $request->start_time, $request->end_time);
        $checkTeacher = $xepPhongObj->checkUpdateExistTeacher($request->id, $request->teacher_id, $request->day, $request->start_time, $request->end_time);

        $body = [
            'id' => $request->id,
            'data' => [
                'class_id' => $request->class_id,
                'room_id' => $request->room_id,
                'teacher_id' => $request->teacher_id,
                'day' => $request->day,
                'mo_ta' => $request->mo_ta,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
            ]
        ];

        if ($record != null && $record->id == $request->id) {
            if ($record->end_time == $request->end_time) {
                if ($checkRoom) {
                    return response()->json([
                        'status' => 500,
                        'heading' => 'Phòng đã được xếp lịch',
                        'error' => $checkRoom
                    ], 500);
                }

                if ($checkTeacher) {
                    return response()->json([
                        'status' => 500,
                        'heading' => 'Giáo viên đã được xếp lịch',
                        'error' => $checkTeacher
                    ], 500);
                }

                $update = $xepPhongObj->saveUpdate($body);

                return response()->json([
                    'status' => 200,
                    'heading' => 'Cập nhật thành công',
                    'data' => $update,
                ], 200);
            } else {
                if ($checkRoom) {
                    return response()->json([
                        'status' => 500,
                        'heading' => 'Phòng đã được xếp lịch',
                        'error' => $checkRoom
                    ], 500);
                }

                if ($checkTeacher) {
                    return response()->json([
                        'status' => 500,
                        'heading' => 'Giáo viên đã được xếp lịch',
                        'error' => $checkTeacher
                    ], 500);
                }

                $update = $xepPhongObj->saveUpdate($body);
                return response()->json([
                    'status' => 200,
                    'heading' => 'Cập nhật thành công',
                    'data' => $update,
                ], 200);
            }
        } else {
            if ($checkRoom) {
                return response()->json([
                    'status' => 500,
                    'heading' => 'Phòng đã được xếp lịch',
                    'error' => $checkRoom
                ], 500);
            }

            if ($checkTeacher) {
                return response()->json([
                    'status' => 500,
                    'heading' => 'Giáo viên đã được xếp lịch',
                    'error' => $checkTeacher
                ], 500);
            }

            $update = $xepPhongObj->saveUpdate($body);
            return response()->json([
                'status' => 200,
                'heading' => 'Cập nhật thành công',
                'data' => $update
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete =  XepPhong::destroy($id);
        if ($delete) {
            return response()->json([
                'status' => true,
                'heading' => "Xoá thành công",
                'data' => $delete
            ], 200);
        }
    }
    public function checkRoomAndTeacher(Request $request)
    {
        $rules = [
            'day' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'heading' => 'validation error',
                'error' => $validator->errors()
            ], 400);
        }
        $objXepPhong = new XepPhong();
        $checkValue = optional($objXepPhong->findRecordByDay($request->day, $request->start_time, $request->end_time))->toArray();
        // $checkValue = optional($objXepPhong->checkCa($request->day, $request->room_id))->toArray();

        return response()->json([
            'status' => 200,
            'heading' => "success",
            'data' => $checkValue['data']
        ], 200);
    }

    public function checkCaHocByRoom(Request $request)
    {
        $rules = [
            'day' => 'required',
            'room_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'heading' => 'validation error',
                'error' => $validator->errors()
            ], 400);
        }
        $objXepPhong = new XepPhong();
        $checkValue = optional($objXepPhong->checkCa($request->day, $request->room_id))->toArray();

        return response()->json([
            'status' => 200,
            'heading' => "success",
            'data' => $checkValue['data']
        ], 200);
    }
}
