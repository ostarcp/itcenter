<?php

namespace App\Http\Requests\ThuRequest;

use Illuminate\Foundation\Http\FormRequest;

class ThuAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'tên thuộc tính' => 'quy định điều kiện'
            'name' => 'bail|required|max:200|unique:roles',
        ];
    }
    //câu thông báo lỗi
    public function messages()
    {
        return [
            'name.required' => 'Thứ không được để trống',
            'name.max' => 'Thứ không vượt quá 200 kí tự',
            'name.unique' => 'Thứ đã tồn tại',
        ];
    }
}
