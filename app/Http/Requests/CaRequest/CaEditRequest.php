<?php

namespace App\Http\Requests\CaRequest;
use App\Rules\CaUpdateUniqueRequest;
use Illuminate\Foundation\Http\FormRequest;

class CaEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'tên thuộc tính' => 'quy định điều kiện'
            'ca_hoc' => ['bail','required',new CaUpdateUniqueRequest()],
            'start_time' => ['required'],
            'end_time' => ['required'],
        ];
    }
    //câu thông báo lỗi
    public function messages()
    {
        return [
            'ca_hoc.required' => 'Thời gian bắt đầu không được để trống',
            'start_time.required' => 'Thời gian bắt đầu không được để trống',
            'end_time.required' => 'Thời gian kết thúc không được để trống',
        ];
    }
}
