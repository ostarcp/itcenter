<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class ClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $dataRequest = $this->request->all();

        Session::push('post_form_data', $dataRequest);

        $currentAction = $this->route()->getActionMethod();
        // dd($this->method());
        switch ($this->method()):
            case 'POST':
                switch ($currentAction) {
                    case 'addClass':
                        $rules = [
                            "name" => "required|min:5|max:199|unique:class",
                            "slot" => "required|numeric",
                            "chi_tieu" => "required|numeric",
                            "start_date" => "required",
                            "end_date" => "required",
                            "lecturer_id" => "required",
                            "location_id" => "required",
                            "course_id" => "required",
                            "chi_tieu" => "required|numeric",
                            "id_thu" => "required",
                        ];
                        break;
                    case 'updateClass':
                        $rules = [
                            "name" => "required",
                            "slot" => "required|numeric",
                            "chi_tieu" => "required|numeric",
                            "start_date" => "required",
                            "end_date" => "required",
                            "lecturer_id" => "required",
                            "location_id" => "required",
                            "course_id" => "required",
                            "chi_tieu" => "required",
                            "id_thu" => "required",
                        ];
                        break;

                    default:
                        break;
                }
                break;
            default:
                break;
        endswitch;

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Bắt buộc phải nhập tên lớp học',
            'name.min' => 'Tên lớp học phải nhập tối thiểu 5 kí tự',
            'name.max' => 'Tên lớp học phải nhập tối đa 199 kí tự',
            'name.unique' => 'Tên lớp học đã tồn tại trên hệ thống',
            'slot.required' => 'Bắt buộc phải nhập số chỗ',
            'chi_tieu.required' => 'Bắt buộc phải nhập chỉ tiêu',
            'start_date.required' => 'Bắt buộc phải nhập ngày bắt đầu',
            'end_date.required' => 'Bắt buộc phải nhập ngày kết thúc',
            'location_id.required' => 'Bắt buộc phải nhập địa điểm',
            'course_id.required' => 'Bắt buộc phải nhập khóa học',
            'chi_tieu.required' => 'Bắt buộc phải chỉ tiêu',
            'chi_tieu.numeric' => 'Bắt buộc phải nhập khóa học',
            'id_thu.required' => 'Bắt buộc phải nhập thứ',
            "lecturer_id.required" => "Bắt buộc phải chọn giảng viên",

        ];
    }
}
