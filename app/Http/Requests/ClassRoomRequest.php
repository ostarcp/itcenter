<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class ClassRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $dataRequest = $this->request->all();

        Session::push('post_form_data', $dataRequest);

        $currentAction = $this->route()->getActionMethod();
        switch ($this->method()):
            case 'POST':
                switch ($currentAction) {
                    case 'AddClassRoom':
                        $rules = [
                            "name" => "required|unique:class_rooms",
                            "description" => "required",
                            'location_id' => 'required',
                        ];
                        break;
                    case 'updateClassRoom':
                        $rules = [
                            "name" => "required|unique:class_rooms",
                            "description" => "required",
                            'location_id' => 'required',
                        ];
                        break;

                    default:
                        break;
                }
                break;
            default:
                break;
        endswitch;

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên phòng học không được để trống',
            'name.unique' => 'Tên Phòng học đã tồn tại trên hệ thống',
            'description.required' => 'mô tả không được để trống',
            'location_id.required' => 'cơ sở không được để trống',
        ];
    }
}
