<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Thu extends Model
{
    protected $table = 'thus';
    protected $fillable = ['name','key','created_at','updated_at'];

    public function loadListIdAndName($where = null){
        $list = DB::table($this->table)->select('id', 'name','key');
        if($where != null)
            $list->where([$where]);
        return $list->get();
    }
}
