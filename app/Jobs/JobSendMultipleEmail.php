<?php

namespace App\Jobs;

use App\HocVien;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
// use Mail;
use Illuminate\Support\Facades\Mail;
use  App\Mail\MailSendMultiple;

class JobSendMultipleEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $listSv;
    protected $lop;
    public function __construct($listSv,$lop)
    {
        $this->listSv = $listSv;
        $this->lop = $lop;
    }

    public function handle()
    {
        // $listSv = $this->listSv;
        // dd($this->listSv);
        // $objDemo = "cc";
        $listSv = $this->listSv;
        $lop = $this->lop;
        foreach ($listSv as $email) {
            Mail::to($email->email)->send(new MailSendMultiple($lop));
        }
    }
}
