<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXepPhongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xep_phong', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('class_id')->nullable();
            $table->unsignedInteger('room_id')->nullable();
            $table->unsignedInteger('teacher_id')->nullable();
            $table->string('day');
            $table->double('start_time')->nullable();
            $table->double('end_time')->nullable();
            $table->string('mo_ta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xep_phong');
    }
}
