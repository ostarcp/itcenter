<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCollumTokenInTableHocVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hoc_vien', function (Blueprint $table) {
            $table->string('token_reset_password')->nullable();
            $table->string('token_expire_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hoc_vien', function (Blueprint $table) {
            $table->dropColumn('token_reset_password');
            $table->dropColumn('token_expire_date');
        });
    }
}
