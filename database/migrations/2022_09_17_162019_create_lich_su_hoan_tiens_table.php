<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLichSuHoanTiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lich_su_hoan_tiens', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_hoc_vien');
            $table->bigInteger('id_user');
            $table->integer('so_tien');
            $table->date('ngay_hoan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lich_su_hoan_tiens');
    }
}
