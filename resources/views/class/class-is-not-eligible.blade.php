@extends('templates.layout')
@section('content')


<!-- Main content -->
<section class="content appTuyenSinh">
    <link rel="stylesheet" href="{{ asset('default/bower_components/select2/dist/css/select2.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('default/bower_components/bootstrap-daterangepicker/daterangepicker.css') }} ">

    <style>
        .select2-container--default .select2-selection--single,
        .select2-selection .select2-selection--single {
            padding: 3px 0px;
            height: 30px;
        }

        .select2-container {
            margin-top: -5px;
        }

        option {
            white-space: nowrap;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 0px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #216992;
        }

        .select2-container--default .select2-selection--multiple {
            margin-top: 10px;
            border-radius: 0;
        }

        .select2-container--default .select2-results__group {
            background-color: #eeeeee;
        }
    </style>
    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <strong>{{ Session::get('success') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
    @endif
    <!-- Phần nội dung riêng của action  -->
    <div class="box box-primary" style="margin-top: 50px">
        <div class="box-header with-border">
            <div class="box-title">
                Danh Sách Lớp Học Không Đủ Chỉ Tiêu Để Khai Giảng
            </div>
        </div>
        <div class="box-body">
            <div class="clearfix"></div>
            <div v-if="list_hoa_dons.length>0" class="table-responsive">
                <table class="table table-bordered" style="margin-top:20px;">
                    <tbody>
                        <tr>
                            <th>#ID</th>
                            <th>Tên lớp học</th>
                            <th>Giá</th>
                            <th>Ngày bắt đầu</th>
                            <th>Ngày kết thúc</th>
                            <th>Giảng viên</th>
                            <th>Địa điểm</th>
                            <th>Khóa học</th>
                            <th>Số chỗ</th>
                            <th>Số chỗ đã đăng kí</th>
                            <th>Chỉ tiêu</th>
                            <th>Trạng thái</th>
                            @canany(['class-add','class-edit'])
                            <th>Công cụ</th>
                            @endcanany
                        </tr>
                        @foreach ($lopKhongDuChiTieu as $lopItem)
                        <tr>
                            <td>
                                {{$lopItem->id}}
                            </td>
                            <td>
                                <a href="{{ route('route_BackEnd_danh_sach_sinh_vien_can_gui_mail_thong_bao',['id'=>$lopItem->id]) }}"> {{$lopItem->name}} </a>
                            </td>
                            <td>
                                {{ number_format($lopItem->course->price)}} VNĐ
                            </td>
                            <td>
                                {{$lopItem->start_date}}
                            </td>
                            <td>
                                {{$lopItem->end_date}}
                            </td>
                            <td>
                                @foreach($listGiangVien as $itemGiangVien)
                                @if( $lopItem->lecturer_id == $itemGiangVien->id )
                                {{$itemGiangVien->name}}
                                @endif
                                @endforeach
                            </td>

                            <td>
                                @foreach($listCoSo as $itemCoSo)
                                @if( $lopItem->location_id == $itemCoSo->id )
                                {{$itemCoSo->name}}
                                @endif
                                @endforeach
                            </td>

                            <td>
                                {{$lopItem->course->name}}
                            </td>
       
                            <td>
                                {{$lopItem->slotBanDau}}
                            </td>
                            <td>
                                {{$lopItem->slot}}
                            </td>
                            <td>
                                {{$lopItem->chi_tieu}}
                            </td>
                            <td>
                                <p style='color:red;'> Không đủ điểu kiện khai giảng </p>

                            </td>
                            @canany(['class-add','class-edit'])
                            <td>
                                <button> <a href=" {{route('route_BackEnd_Class_sua_lop_khong_du_dieu_kien',['id'=>$lopItem->id]) }}" onclick="return  confirm('Bạn có chắc chắn cập nhập thời gian khai giảng')"> Cập nhập </a> </button>
                            </td>
                            @endcanany
                        </tr>
                        @endforeach

                    </tbody>

                </table>
            </div>

        </div>
        <br>

    </div>

    <index-cs ref="index_cs"></index-cs>
</section>
@endsection
@section('script')
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
{{-- <script src="public/default/plugins/input-mask/jquery.inputmask.extensions.js"></script> --}}
<script src="{{ asset('default/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('default/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/taisan.js') }} "></script>
<script src="{{ asset('js/khoahoc.js') }} "></script>
@endsection