@extends('templates.layout')
@section('content')


<!-- Main content -->
<section class="content appTuyenSinh">
    <link rel="stylesheet" href="{{ asset('default/bower_components/select2/dist/css/select2.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('default/bower_components/bootstrap-daterangepicker/daterangepicker.css') }} ">

    <style>
        .select2-container--default .select2-selection--single,
        .select2-selection .select2-selection--single {
            padding: 3px 0px;
            height: 30px;
        }

        .select2-container {
            margin-top: -5px;
        }

        option {
            white-space: nowrap;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #aaa;
            border-radius: 0px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #216992;
        }

        .select2-container--default .select2-selection--multiple {
            margin-top: 10px;
            border-radius: 0;
        }

        .select2-container--default .select2-results__group {
            background-color: #eeeeee;
        }
    </style>

    <!-- Phần nội dung riêng của action  -->
    <div class="box box-primary" style="margin-top: 50px">
        <div class="box-header with-border">
            <div class="box-title">
                Danh Sách Sinh Viên thuộc  {{$lop->name}} không đủ điều kiện khai giảng
            </div>
        </div>
        <div class="box-body">
            <div class="clearfix"></div>
            <div v-if="list_hoa_dons.length>0" class="table-responsive">
                <table class="table table-bordered" style="margin-top:20px;">
                    <tbody>
                        <tr>
                            <th>#ID</th>
                            <th>Tên Sinh viên</th>
                            <th>Ngày sinh</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                        </tr>
                        @foreach($lop->sv as $sv)

                        @foreach( $sv as $svItem )
                            <tr>
                            <td>
                                {{$svItem->id}}
                            </td>
                            <td>
                                {{$svItem->ho_ten}}
                            </td>
                            <td>
                                {{$svItem->ngay_sinh}}
                            </td>
                            <td>
                                {{$svItem->so_dien_thoai}}
                            </td>
                            <td>
                                {{$svItem->email}}
                            </td>
                        </tr>

                        
                        @endforeach
                        @endforeach


                    </tbody>

                </table>
            </div>

        </div>
        <br>

    </div>

    <index-cs ref="index_cs"></index-cs>
</section>
@endsection
@section('script')
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
{{-- <script src="public/default/plugins/input-mask/jquery.inputmask.extensions.js"></script> --}}
<script src="{{ asset('default/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('default/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/taisan.js') }} "></script>
<script src="{{ asset('js/khoahoc.js') }} "></script>
@endsection