@extends('templates.layout')
@section('title', '1233')

@section('script')
<link rel="stylesheet" href="{{ asset('default/bower_components/select2/dist/css/select2.min.css') }} ">
<link rel="stylesheet" href="{{ asset('default/bower_components/bootstrap-daterangepicker/daterangepicker.css') }} ">
<link rel="stylesheet" href="{{ asset('default/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }} ">

@section('css')
<style>
    body {
        /*-webkit-touch-callout: none;
                                                                                                                                                                -webkit-user-select: none;
                                                                                                                                                                -moz-user-select: none;
                                                                                                                                                                -ms-user-select: none;
                                                                                                                                                                -o-user-select: none;*/
        user-select: none;
    }

    .toolbar-box form .btn {
        /*margin-top: -3px!important;*/
    }

    .select2-container {
        margin-top: 0;
    }

    .select2-container--default .select2-selection--multiple {
        border-radius: 0;
    }

    .select2-container .select2-selection--multiple {
        min-height: 30px;
    }

    .select2-container .select2-search--inline .select2-search__field {
        margin-top: 3px;
    }

    .table>tbody>tr.success>td {
        background-color: #009688;
        color: white !important;
    }

    .table>tbody>tr.success>td span {
        color: white !important;
    }

    .table>tbody>tr.success>td span.button__csentity {
        color: #333 !important;
    }

    /*.table>tbody>tr.success>td i{*/
    /*    color: white !important;*/
    /*}*/
    .text-silver {
        color: #f4f4f4;
    }

    .btn-silver {
        background-color: #f4f4f4;
        color: #333;
    }

    .select2-container--default .select2-results__group {
        background-color: #eeeeee;
    }

    .select2-container--default .select2-selection--single,
    .select2-selection .select2-selection--single {
        padding: 3px 0px;
        height: 30px;
    }

    .select2-container {
        margin-top: -5px;
    }

    option {
        white-space: nowrap;
    }

    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 0px;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        color: #216992;
    }

    .select2-container--default .select2-selection--multiple {
        margin-top: 10px;
        border-radius: 0;
    }

    .select2-container--default .select2-results__group {
        background-color: #eeeeee;
    }
</style>
@endsection
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    @include('templates.header-action')


</section>

<!-- Main content -->
<section class="content appTuyenSinh">
    <div id="msg-box">
        <?php //Hiển thị thông báo thành công
        ?>
        @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <strong>{{ Session::get('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        @endif
        <?php //Hiển thị thông báo lỗi
        ?>
        @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        @endif
    </div>
    @if(!isset($lichDay))
    <p class="alert alert-warning">
        Không có dữ liệu phù hợp
    </p>
    @endif
    <div class="box-body table-responsive no-padding">
        <form action="" method="post">
            @csrf

            <div class="clearfix"></div>
            <div class="double-scroll">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 50px" class="text-center">
                            STT
                        </th>
                        <th class="text-center">Ngày</th>
                        <th class="text-center">Phòng</th>
                        <th class="text-center">lớp</th>
                        <th class="text-center">Ca / Thời gian</th>
                    </tr>

                    @foreach ($lich_day as $lich)

                    <tr>
                        <td class="text-center" style="vertical-align:middle ;">1</td>
                        <td class="text-center" style="vertical-align:middle ;">{{$lich->day}}</td>
                        <td class="text-center" style="vertical-align:middle ;">
                            {{$lich->room_id}}
                        </td>
                        <td class="text-center" style="vertical-align:middle ;">{{$lich->class_id}}</td>
                        <td class="text-center" style="vertical-align:middle ;">{{$lich->mo_ta}}</td>

                    </tr>
                    @endforeach

                </table>
            </div>
        </form>
    </div>
    <br>
    <div class="text-center">
    </div>
    <index-cs ref="index_cs"></index-cs>
</section>

@endsection

@section('script')
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
{{-- <script src="public/default/plugins/input-mask/jquery.inputmask.extensions.js"></script> --}}
<script src="{{ asset('default/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('default/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('default/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
{{-- <script>
        $(function() {
            $('input[name="search_ngay_dang_ky"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: parseInt(moment().format('YYYY'), 10),
                autoApply: false,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        });
    </script> --}}

<script src="{{ asset('js/taisan.js') }} "></script>


@endsection