@extends('templates.layout')
@section('content')
<div class="content-wrappers">

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif
                <form action="{{route('route_BackEnd_thu_store')}}" method="post" style="width:100%">
                    @csrf
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Thêm thứ</label>
                            <input type="text" @error('name') is-invalid @enderror name="name" value="{{old('name')}}" class="form-control" placeholder="Nhập thứ học">
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div>
@endsection
@section('script')
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('default/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/khoahoc.js') }} "></script>
@endsection