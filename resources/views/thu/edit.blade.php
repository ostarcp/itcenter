@extends('templates.layout')
@section('content')
<div class="content-wrappers">
    
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}
                        </div>
                    @endif
                <form action="{{route('route_BackEnd_thu_update',['id'=>$thuEdit->id])}}" method="post" style="width:100%">
                    @csrf
                    @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Thứ</label>
                            <input type="text" name="name" value="{{$thuEdit->name}}" class="form-control" placeholder="Nhập thứ">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div>
@endsection
@section('script')
    <script>
        $('.checkbox_wrapper').on('click', function() {
        //$(this) chính là checkbox . tìm thàng cha là card. sau đó tìm đến checkbox_children và thêm thuộc tính prop (prop trả về true hoặc false) và nếu checked thằng cha thì  $(this).prop('checked') là true và sẽ tự thêm checked
        $(this).parents('.card').find('.checkbox_children').prop('checked', $(this).prop('checked'));
        })

    </script>
    <script src="{{ asset('default/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('default/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('js/khoahoc.js') }} "></script>
@endsection

